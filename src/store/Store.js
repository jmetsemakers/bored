import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export const store = new Vuex.Store({
    state: {
        filters: {
            accessibility: null,
            type: null,
            participants: null,
            price: null
        },
        message: null,
        activity: null,
        page: "activity",
        pages: ["activity", "filtered-activity", "increasing-activity"],
        finished: false,
    },
    mutations: {
        updateFilters(state, value) {
          state.filters = value;
        },
        resetFilters(state) {
            state.filters = {
                accessibility: null,
                type: null,
                participants: null,
                price: null
            };
        },
        setActivity(state, value) {
            state.activity = value;
            state.message = null;
        },
        setMessage(state, value) {
            state.message = value;
            state.activity = null;
        },
        setPage(state, value) {
            state.page = value;
        },
        setIncrease(state, value) {
            state.filters = {
                accessibility: value
            }; 
        },
        setFinished (state, value) {
            state.finished = value;
        }
    }
  })